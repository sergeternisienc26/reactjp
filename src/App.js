import "./App.css";
import justeprix from "./justeprix.png";
import villa from "./villa.jpg";
import { useState, useEffect } from "react";

function App() {
  const [numbAlea, setNumbAlea] = useState(() =>
    Math.floor(Math.random() * 500001)
  );
  const [number, setNumber] = useState("");
  const [text, setText] = useState("");
  const [count, setCount] = useState(0);
  const [restart, setRestart] = useState("inactive");
  const [timer, setTimer] = useState(60);
  const [interval, setTimerInterval] = useState(null);
  const [stateTimer, setStateTimer] = useState("inactive");
  const [options, setOptions] = useState("inactive");

  function handleChangeNumber(e) {
    setNumber(e.target.value);
  }

  function handleClick() {
    setCount(count + 1);
    setStateTimer("active");
    console.log(number, numbAlea);
    if (Number(number) === numbAlea) {
      setText("C'est gagné");
      clearInterval(interval);
      setRestart("active");
    } else if (Number(number) < numbAlea) {
      setText("C'est plus");
    } else {
      setText("C'est moins");
    }
    if (options === "active") {
      setCount(count - 1);
      setStateTimer("inactive");
      if (count === 0){
        setCount(count);
        setRestart("active");
        setOptions("inactive");
      }
    }
  }

  useEffect(() => {
    if (stateTimer === "active") {
      setTimerInterval(
        setInterval(() => {
          setTimer((timer) => timer - 1);
        }, 1000)
      );
      return () => clearInterval(interval);
    }
  }, [stateTimer]);

  useEffect(() => {
    if (timer === 0) {
      alert(
        `Vous avez perdu, voici le nombre qui était recherché : ${numbAlea}`
      );
      clearInterval(interval);
      setRestart("active");
    }
  });

  function refreshPage() {
    setNumber("");
    setNumbAlea(() => Math.floor(Math.random() * 500000));
    setText("");
    setCount(0);
    setRestart("inactive");
    setStateTimer("inactive");
    setTimer(60);
  }

  function showText() {
    return <h1>{text}</h1>;
  }

  function test() {
    
    if (options === 'active'){
      setOptions("inactive");
      setCount(0);
    } else {
      setOptions("active");
    }
  }

  function handleGetValue(e) {
    setCount(e.target.value);
  }

  return (
    <div className="App">
      <header className="App-header">
        <img className="logo" src={justeprix} alt="logo jp" />
      </header>
      
      <body>
      <div className="interface">
        <div className="boxButton">
          {showText()}
          <p className="tries">Nombre essais : {count}</p>
          <input
            type="text"
            value={number}
            onChange={handleChangeNumber}
            maxLength="6"
          />
          <button onClick={handleClick}>envoyer</button>
          <button className={`button ${restart}`} onClick={refreshPage}>
            Recommencer
          </button>
          <div>
            <label>Choisir le nombre d'essais:</label>
            <input className="checkbox" type="checkbox" onClick={test} />
            <input
              type="text"
              className={`select ${options}`}
              onChange={handleGetValue}
              maxLength="3"
            />
          </div>
        </div>
        <p className="timer">Temps : {timer} s</p>
        <div className="photos">
          <img className="villa" src={villa} alt="villa" />
        </div>
      </div>
      </body>
    </div>
  );
}

export default App;
